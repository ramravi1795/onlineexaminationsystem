<?php
namespace App\Service;
use App\Entity\User;
use App\Entity\Question;
use App\Entity\RefRole;
use App\Entity\IdGenerator;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use App\Utils\ApiResponse;
use Symfony\Component\Validator\Constraints\DateTime;

class OnlineExaminationSystemService
{

    private $EM;
    public function __construct(EntityManagerInterface $EM)
    {
        $this->EM = $EM;
    }


    public function _addUserRegistration($request)
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $content = $request->getContent();
        $data = $serializer->deserialize($content, User::class, 'json');

        $AutoId = $this->IdGenerator("user");
        $password = $data->getPassword();
        $confirmpassword = $data->getConfirmPassword();
        $rolerepo = $this->EM->getRepository(RefRole::class);
        $objrole = $rolerepo->findOneBy(['id' => $data->getRoleId()]);
        if ($objrole == null) {
            return "Ko";
        }

        $user = new User;
        $user->setUserName($AutoId);
        $user->setName($data->getName());
        $user->setRole($objrole);
        $user->setContactNo($data->getContactNo());
        $user->setEmailId($data->getEmailId());
        $user->setPassword($password);
        if($password == $confirmpassword){
            $user->setConfirmPassword($password);
            } else {
            return "The Password And ConfirmPassword is Not Same";
            }    
        $user->setIsActive(true);
        $this->EM->persist($user);
        $this->EM->flush();
        return $user;
    }

    public function _updateUser($user_id, $request)
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $content = $request->getContent();
        $data = $serializer->deserialize($content, User::class, 'json');
        $userrepo = $this->EM->getRepository(User::class);
        $objuser = $userrepo->findOneBy(['id' => $user_id]);
        if ($objuser == null) {
            return "Ko";
        }
        $rolerepo = $this->EM->getRepository(RefRole::class);
        $objrole = $rolerepo->findOneBy(['id' => $data->getRoleId()]);
        if ($objrole == null) {
            return "Ko";
        }
        $objuser->setName($data->getName());
        $objuser->setRole($objrole);
        $objuser->setContactNo($data->getContactNo());
        $objuser->setEmailId($data->getEmailId());
        $objuser->setPassword($data->getPassword());
        $objuser->setConfirmPassword($data->getConfirmPassword());
        $objuser->setIsActive($data->getIsActive());
        $this->EM->persist($objuser);
        $this->EM->flush();
        return $objuser;
    }

    public function _updateUserStatusChange($user_id, $status)
    {
        $userrepo = $this->EM->getRepository(User::class);
        $objuser = $userrepo->findOneBy(['id' => $user_id]);
        if ($objuser == null) {
            return "Ko";
        }
        if ($status == 'enable') {
            $objuser->setIsActive(true);
            $this->EM->persist($objuser);
            $this->EM->flush();
            return $objuser;
        }
        if ($status == 'disable') {
            $objuser->setIsActive(false);
            $this->EM->persist($objuser);
            $this->EM->flush();
            return $objuser;
        } else {
            return "Error";
        }
    }

    public function _getSingleUser($user_id)
    {
        $userrepo = $this->EM->getRepository(User::class);
        $objuser = $userrepo->findOneBy(['id' => $user_id]);
        if ($objuser == null) {
            return "Ko";
        }
        return $objuser;
    }

    public function _getUserDetailsWithRole($role_id)
    {
        $userrepo = $this->EM->getRepository(User::class);
        $objuser = $userrepo->findBy(['role' => $role_id]);
        if ($objuser == null) {
            return "Ko";
        }
        return $objuser;
    }

    public function _getAllUser()
    {
        $userrepo = $this->EM->getRepository(User::class);
        $objuser = $userrepo->findAll();
        if ($objuser == null) {
            return "Ko";
        }
        return $objuser;
    }

    public function _DeleteUserDetails($user_id)
    {
        $userrepo = $this->EM->getRepository(User::class);
        $objusers = $userrepo->findOneBy(['id' => $user_id]);
        if ($objusers == null) {
            return "Ko";
        }
        $this->EM->remove($objusers);
        $this->EM->flush();
        return "Delete Successfull";
    }

    public function IdGenerator($code)
    {
        $Idgeneratorrepo = $this->EM->getRepository(IdGenerator::class);
        $generate = $Idgeneratorrepo->findOneBy(['code' => $code]);
        $generate->setvalue($generate->getvalue() + 1);
        $this->EM->persist($generate);
        $this->EM->flush();
        return sprintf("%s%0" . $generate->getLength() . "d%s", $generate->getPrefix(), $generate->getValue(), $generate->getSuffix());
    }

    public function _loginUser($request){
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $content = $request->getContent();
        $data = $serializer->deserialize($content, User::class, 'json');

        $username = $data->getUserName();
        $password = $data->getPassword();

        $datauserrepo = $this->EM->getRepository(User::class);
        $objDataUser = $datauserrepo->findOneBy(['userName'=>$username,'password'=>$password]);
        if(!$objDataUser){
            return "Login Not user";  
        } else{
            $UserName = $objDataUser->getUserName();
            $Name = $objDataUser->getName();
            $Role = $objDataUser->getRole();
            $map["UserName"] = $UserName;
            $map["Name"] = $Name;
            $map["Role"] = $Role;
            return $map;
        }
    }

    public function _addQuestion($request)
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $content = $request->getContent();
        $data = $serializer->deserialize($content, Question::class, 'json');
       
        $question = new Question;
        $question->setQuestion($data->getQuestion());
        $question->setOption1($data->getOption1());
        $question->setOption2($data->getOption2());
        $question->setOption3($data->getOption3());
        $question->setAnswer($data->getAnswer());
        $this->EM->persist($question);
        $this->EM->flush();
        return $question;
    }

    public function _updateQuestion($question_id, $request)
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $content = $request->getContent();
        $data = $serializer->deserialize($content, Question::class, 'json');
        $questionrepo = $this->EM->getRepository(Question::class);
        $objquestion = $questionrepo->findOneBy(['id' => $question_id]);
        if ($objquestion == null) {
            return "Ko";
        }
        $objquestion->setQuestion($data->getQuestion());
        $objquestion->setOption1($data->getOption1());
        $objquestion->setOption2($data->getOption2());
        $objquestion->setOption3($data->getOption3());
        $objquestion->setAnswer($data->getAnswer());
        $this->EM->persist($objquestion);
        $this->EM->flush();
        return $objquestion;
    }

    public function _updateQuestionStatusChange($question_id, $status)
    {
        $questionrepo = $this->EM->getRepository(Question::class);
        $objquestion = $questionrepo->findOneBy(['id' => $question_id]);
        if ($objquestion == null) {
            return "Ko";
        }
        if ($status == 'enable') {
            $objquestion->setIsActive(true);
            $this->EM->persist($objquestion);
            $this->EM->flush();
            return $objquestion;
        }
        if ($status == 'disable') {
            $objquestion->setIsActive(false);
            $this->EM->persist($objquestion);
            $this->EM->flush();
            return $objquestion;
        } else {
            return "Error";
        }
    }

    public function _getSingleQuestion($question_id)
    {
        $questionrepo = $this->EM->getRepository(Question::class);
        $objquestion = $questionrepo->findOneBy(['id' => $question_id]);
        if ($objquestion == null) {
            return "Ko";
        }
        return $objquestion;
    }

    public function _getAllQuestion()
    {
        $questionrepo = $this->EM->getRepository(Question::class);
        $objquestion = $questionrepo->findAll();
        if ($objquestion == null) {
            return "Ko";
        }
        return $objquestion;
    }

    public function _DeleteQuestion($question_id)
    {
        $questionrepo = $this->EM->getRepository(Question::class);
        $objquestion = $questionrepo->findOneBy(['id' => $question_id]);
        if ($objquestion == null) {
            return "Ko";
        }
        $this->EM->remove($objquestion);
        $this->EM->flush();
        return "Delete Successfull";
    }

    public function _insertRefRole($request){ 
        $encoders = [ new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers ,$encoders);
        $content = $request->getContent();
        $dataRefRole = $serializer->deserialize($content,RefRole::class, 'json');
        $objRefRole = new RefRole;
        $objRefRole->setName($dataRefRole->getName());
        $objRefRole->setDescription($dataRefRole->getDescription());
        $objRefRole->setIsActive(true);
        $this->EM->persist($objRefRole);
        $this->EM->flush();
        return $objRefRole;
    }

    
    public function _updateRefRole($role_id,$request){ 
        $encoders = [ new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers ,$encoders);
        $content = $request->getContent();
        $dataRefRole = $serializer->deserialize($content,RefRole::class, 'json');
        $refrolerepo = $this->EM->getRepository(RefRole::class);
        $objRefRole = $refrolerepo->findOneBy(['id'=>$role_id]);
        if(!$objRefRole){
            return "ko";
        }
        $objRefRole->setName($dataRefRole->getName());
        $objRefRole->setDescription($dataRefRole->getDescription());
        $objRefRole->setIsActive($dataRefRole->getIsActive());
        $this->EM->persist($objRefRole);
        $this->EM->flush();
        return $objRefRole;
    }

    public function _getRefRole($status){
        $refrolerepo = $this->EM->getRepository(RefRole::class);
        $objRefRole = $refrolerepo->findBy(['isActive'=>$status]);
        return $objRefRole;
    }

    public function _getSingleRefRole($role_id){
        $refrolerepo = $this->EM->getRepository(RefRole::class);
        $objRefRole = $refrolerepo->findOneBy(['id'=>$role_id]);
        if($objRefRole == null){
            return "ko";
        }
        return $objRefRole;
    }

    public function _getAllRefRole()
    {
        $rolerepo = $this->EM->getRepository(RefRole::class);
        $objrole = $rolerepo->findAll();
        if ($objrole == null) {
            return "Ko";
        }
        return $objrole;
    }

    public function _DeleteRefRole($role_id)
    {
        $rolerepo = $this->EM->getRepository(RefRole::class);
        $objrole = $rolerepo->findOneBy(['id' => $role_id]);
        if ($objrole == null) {
            return "Ko";
        }
        $this->EM->remove($objrole);
        $this->EM->flush();
        return "Delete Successfull";
    }

    
    public function _getQuestionAndAnswer($request)
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $content = $request->getContent();
        $data = $serializer->deserialize($content, Question::class, 'json');

        $question =  $data->getQuestionId();
        $key = $data->getKeyAnswer();

        $questionrepo = $this->EM->getRepository(Question::class);
        $objQuestion = $questionrepo->findOneBy(['id'=>$question]);
        if($objQuestion){
        $QuestionName = $objQuestion->getQuestion();
        $Answers = $objQuestion->getAnswer();
        if ($Answers == $key){
            $marks = +2;
            $map["Question"] = $QuestionName;
            $map["Answer"] = $Answers;
            $map["Mark"] =  $marks;
        }else {
            $marks = -2;
            $map["Question"] = $QuestionName;
            $map["Answer"] = $Answers;
            $map["Mark"] =  $marks;
        }
        return $map;
        } else {
            return "ko";
        }

    }

}