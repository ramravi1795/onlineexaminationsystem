<?php

namespace App\Repository;

use App\Entity\RefRole;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RefRole|null find($id, $lockMode = null, $lockVersion = null)
 * @method RefRole|null findOneBy(array $criteria, array $orderBy = null)
 * @method RefRole[]    findAll()
 * @method RefRole[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RefRoleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RefRole::class);
    }

    // /**
    //  * @return RefRole[] Returns an array of RefRole objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RefRole
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
