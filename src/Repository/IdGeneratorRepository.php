<?php

namespace App\Repository;

use App\Entity\IdGenerator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method IdGenerator|null find($id, $lockMode = null, $lockVersion = null)
 * @method IdGenerator|null findOneBy(array $criteria, array $orderBy = null)
 * @method IdGenerator[]    findAll()
 * @method IdGenerator[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IdGeneratorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IdGenerator::class);
    }

    // /**
    //  * @return IdGenerator[] Returns an array of IdGenerator objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?IdGenerator
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
