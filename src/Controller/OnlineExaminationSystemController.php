<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\OnlineExaminationSystemService;
use App\Utils\ApiResponse;


class OnlineExaminationSystemController extends AbstractController
{
    /**
     * @Route("/online/examination/system", name="online_examination_system")
     */
    public function index(): Response
    {
        return $this->render('online_examination_system/index.html.twig', [
            'controller_name' => 'OnlineExaminationSystemController',
        ]);
    }

      
    /**
     * @Route("/api/add/user", name="addUserRegistration",methods={"POST"})
     */
    public function addUserRegistration(Request $request, OnlineExaminationSystemService $onlineexaminationsystem)
    {
        $user = $onlineexaminationsystem->_addUserRegistration($request);
        if ($user == "The Password And ConfirmPassword is Not Same"){
            return new ApiResponse("The Password And ConfirmPassword is Not Same", 400, ["Content-Type" => "application/json"], 'json', 'User Not Added', ['timezone']);
        }
        return new ApiResponse($user, 200, ["Content-Type" => "application/json"], 'json', "Success");
    }

    /**
     * @Route("/api/update/user/{user_id}", name="updateUser",methods={"PUT"})
     */
    public function updateUser($user_id, Request $request, OnlineExaminationSystemService $onlineexaminationsystem)
    {
        $user = $onlineexaminationsystem->_updateUser($user_id, $request);
        if ($user == "Ko") {
            return new ApiResponse("resource not found", 400, ["Content-Type" => "application/json"], 'json', 'success', ['timezone']);
        }
        return new ApiResponse($user, 200, ["Content-Type" => "application/json"], 'json', "Success");
    }

    /**
     * @Route("/api/update/user/{user_id}/{status}", name="updateUserStatusChange",methods={"PUT"})
     */
    public function updateUserStatusChange($user_id, $status, OnlineExaminationSystemService $onlineexaminationsystem)
    {
        $user = $onlineexaminationsystem->_updateUserStatusChange($user_id, $status);
        if ($user == "Ko") {
            return new ApiResponse("resource not found", 400, ["Content-Type" => "application/json"], 'json', 'success', ['timezone']);
        }
        return new ApiResponse($user, 200, ["Content-Type" => "application/json"], 'json', "Success");
    }

    /**
     * @Route("/api/get/user/{user_id}", name="getSingleUser",methods={"GET"})
     */
    public function getSingleUser($user_id, OnlineExaminationSystemService $onlineexaminationsystem)
    {
        $user = $onlineexaminationsystem->_getSingleUser($user_id);
        if ($user == "Ko") {
            return new ApiResponse("resource not found", 400, ["Content-Type" => "application/json"], 'json', 'success', ['timezone']);
        }
        return new ApiResponse($user, 200, ["Content-Type" => "application/json"], 'json', "Success");
    }

     /**
     * @Route("/api/get/userrole/{role_id}", name="getUserDetailsWithRole",methods={"GET"})
     */
    public function getUserDetailsWithRole($role_id, OnlineExaminationSystemService $onlineexaminationsystem)
    {
        $user = $onlineexaminationsystem->_getUserDetailsWithRole($role_id);
        if ($user == "Ko") {
            return new ApiResponse("resource not found", 400, ["Content-Type" => "application/json"], 'json', 'success', ['timezone']);
        }
        return new ApiResponse($user, 200, ["Content-Type" => "application/json"], 'json', "Success");
    }

    /**
     * @Route("/api/getall/user", name="getAllUser", methods={"GET"})
     */
    public function getAllUser(OnlineExaminationSystemService $onlineexaminationsystem)
    {
        $user = $onlineexaminationsystem->_getAllUser();
        if ($user == "Ko") {
            return new ApiResponse("resource not found", 400, ["Content-Type" => "application/json"], 'json', 'success', ['timezone']);
        }
        return new ApiResponse($user, 200, ["Content-Type" => "application/json"], 'json', 'success', ['timezone']);
    }
            
    /**
     * @Route("/api/user/loginuser", name="loginUser",methods={"POST"})
     */
    public function loginUser(Request $request, OnlineExaminationSystemService $onlineexaminationsystem)
    {
        $user = $onlineexaminationsystem->_loginUser($request);
        if ($user == "Login Not user"){
            return new ApiResponse("Login Not user", 400, ["Content-Type" => "application/json"], 'json', 'User Not Found', ['timezone']);
        }
        return new ApiResponse($user, 200, ["Content-Type" => "application/json"], 'json', "Login Successfull");
    }

     /**
     * @Route("/api/delete/user/{user_id}", name="DeleteUserDetails", methods={"DELETE"})
     */
    public function DeleteUserDetails($user_id,OnlineExaminationSystemService $onlineexaminationsystem)
    {
        $user = $onlineexaminationsystem->_DeleteUserDetails($user_id);
        if ($user == "Ko") {
            return new ApiResponse("resource not found", 400, ["Content-Type" => "application/json"], 'json', 'success', ['timezone']);
        }
        return new ApiResponse($user, 200, ["Content-Type" => "application/json"], 'json', 'success', ['timezone']);
    }

    /**
     * @Route("/api/add/refrole", name="insertRefRole",methods={"POST"})
     */
    public function insertRefRole(Request $request, OnlineExaminationSystemService $onlineexaminationsystem)
    {
        $refrole = $onlineexaminationsystem->_insertRefRole($request);
        if ($refrole == "Ko") {
            return new ApiResponse("resource not found", 400, ["Content-Type" => "application/json"], 'json', 'success', ['timezone']);
        }
        return new ApiResponse($refrole, 200, ["Content-Type" => "application/json"], 'json', "Success");
    }

    /**
     * @Route("/api/update/refrole/{role_id}", name="updateRefRole",methods={"PUT"})
     */
    public function updateRefRole($role_id, Request $request, OnlineExaminationSystemService $onlineexaminationsystem)
    {
        $refrole = $onlineexaminationsystem->_updateRefRole($role_id, $request);
        if ($refrole == "Ko") {
            return new ApiResponse("resource not found", 400, ["Content-Type" => "application/json"], 'json', 'success', ['timezone']);
        }
        return new ApiResponse($refrole, 200, ["Content-Type" => "application/json"], 'json', "Success");
    }

    /**
     * @Route("/api/get/refrole/{role_id}", name="getSingleRefRole",methods={"GET"})
     */
    public function getSingleRefRole($role_id, OnlineExaminationSystemService $onlineexaminationsystem)
    {
        $option = $onlineexaminationsystem->_getSingleRefRole($role_id);
        if ($option == "Ko") {
            return new ApiResponse("resource not found", 400, ["Content-Type" => "application/json"], 'json', 'success', ['timezone']);
        }
        return new ApiResponse($option, 200, ["Content-Type" => "application/json"], 'json', "Success");
    }

    /**
     * @Route("/api/getall/refrole", name="getAllRefRole", methods={"GET"})
     */
    public function getAllRefRole(OnlineExaminationSystemService $onlineexaminationsystem)
    {
        $option = $onlineexaminationsystem->_getAllRefRole();
        if ($option == "Ko") {
            return new ApiResponse("resource not found", 400, ["Content-Type" => "application/json"], 'json', 'success', ['timezone', "__initializer__", "__cloner__", "__isInitialized__"]);
        }
        return new ApiResponse($option, 200, ["Content-Type" => "application/json"], 'json', 'success', ['timezone', "__initializer__", "__cloner__", "__isInitialized__"]);
    }
    
     /**
     * @Route("/api/delete/refrole/{role_id}", name="DeleteRefRole", methods={"DELETE"})
     */
    public function DeleteRefRole($role_id,OnlineExaminationSystemService $onlineexaminationsystem)
    {
        $user = $onlineexaminationsystem->_DeleteRefRole($role_id);
        if ($user == "Ko") {
            return new ApiResponse("resource not found", 400, ["Content-Type" => "application/json"], 'json', 'success', ['timezone']);
        }
        return new ApiResponse($user, 200, ["Content-Type" => "application/json"], 'json', 'success', ['timezone']);
    }

    
    /**
     * @Route("/api/add/question", name="addOption",methods={"POST"})
     */
    public function addQuestion(Request $request, OnlineExaminationSystemService $onlineexaminationsystem)
    {
        $question = $onlineexaminationsystem->_addQuestion($request);
        if ($question == "Ko") {
            return new ApiResponse("resource not found", 400, ["Content-Type" => "application/json"], 'json', 'success', ['timezone', "__initializer__", "__cloner__", "__isInitialized__"]);
        }
        return new ApiResponse($question, 200, ["Content-Type" => "application/json"], 'json', "Success");
    }

    /**
     * @Route("/api/update/question/{question_id}", name="updateQuestion",methods={"PUT"})
     */
    public function updateQuestion($question_id, Request $request, OnlineExaminationSystemService $onlineexaminationsystem)
    {
        $question = $onlineexaminationsystem->_updateQuestion($question_id, $request);
        if ($question == "Ko") {
            return new ApiResponse("resource not found", 400, ["Content-Type" => "application/json"], 'json', 'success', ['timezone', "__initializer__", "__cloner__", "__isInitialized__"]);
        }
        return new ApiResponse($question, 200, ["Content-Type" => "application/json"], 'json', "Success");
    }

    /**
     * @Route("/api/update/question/{question_id}/{status}", name="updateQuestionStatusChange",methods={"PUT"})
     */
    public function updateQuestionStatusChange($question_id, $status, OnlineExaminationSystemService $onlineexaminationsystem)
    {
        $question = $onlineexaminationsystem->_updateQuestionStatusChange($question_id, $status);
        if ($question == "Ko") {
            return new ApiResponse("resource not found", 400, ["Content-Type" => "application/json"], 'json', 'success',['timezone', "__initializer__", "__cloner__", "__isInitialized__"]);
        }
        return new ApiResponse($question, 200, ["Content-Type" => "application/json"], 'json', "Success");
    }

    /**
     * @Route("/api/get/question/{question_id}", name="getSingleQuestion",methods={"GET"})
     */
    public function getSingleQuestion($question_id, OnlineExaminationSystemService $onlineexaminationsystem)
    {
        $question = $onlineexaminationsystem->_getSingleQuestion($question_id);
        if ($question == "Ko") {
            return new ApiResponse("resource not found", 400, ["Content-Type" => "application/json"], 'json', 'success', ['timezone', "__initializer__", "__cloner__", "__isInitialized__"]);
        }
        return new ApiResponse($question, 200, ["Content-Type" => "application/json"], 'json', "Success");
    }

    /**
     * @Route("/api/getall/question", name="getAllQuestion", methods={"GET"})
     */
    public function getAllQuestion(OnlineExaminationSystemService $onlineexaminationsystem)
    {
        $question = $onlineexaminationsystem->_getAllQuestion();
        if ($question == "Ko") {
            return new ApiResponse("resource not found", 400, ["Content-Type" => "application/json"], 'json', 'success', ['timezone', "__initializer__", "__cloner__", "__isInitialized__"]);
        }
        return new ApiResponse($question, 200, ["Content-Type" => "application/json"], 'json', 'success', ['timezone', "__initializer__", "__cloner__", "__isInitialized__"]);
    }
    
     /**
     * @Route("/api/delete/question/{question_id}", name="DeleteQuestion", methods={"DELETE"})
     */
    public function DeleteQuestion($question_id,OnlineExaminationSystemService $onlineexaminationsystem)
    {
        $question = $onlineexaminationsystem->_DeleteQuestion($question_id);
        if ($question == "Ko") {
            return new ApiResponse("resource not found", 400, ["Content-Type" => "application/json"], 'json', 'success', ['timezone', "__initializer__", "__cloner__", "__isInitialized__"]);
        }
        return new ApiResponse($question, 200, ["Content-Type" => "application/json"], 'json', 'success', ['timezone', "__initializer__", "__cloner__", "__isInitialized__"]);
    }

  
    /**
     * @Route("/api/mark/questionandanswer", name="getQuestionAndAnswer",methods={"POST"})
     */
    public function getQuestionAndAnswer(Request $request, OnlineExaminationSystemService $onlineexaminationsystem)
    {
        $question = $onlineexaminationsystem->_getQuestionAndAnswer($request);
        if ($question == "Ko") {
            return new ApiResponse("resource not found", 400, ["Content-Type" => "application/json"], 'json', 'success', ['timezone', "__initializer__", "__cloner__", "__isInitialized__"]);
        }
        return new ApiResponse($question, 200, ["Content-Type" => "application/json"], 'json', "Success");
    }

}
