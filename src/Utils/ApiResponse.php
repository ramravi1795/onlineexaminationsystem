<?php
namespace App\Utils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;

class ApiResponse extends Response{
    public function __construct($content = '', int $status = 200, array $headers = [],$type='json', $msg='',$ignoreType= [],float $remaining=0)
    {
        $encoders = [ new JsonEncoder()];
        
        // $objNormaliser=new ObjectNormalizer();
        // $objNormaliser->setCircularReferenceLimit(1);
        // $objNormaliser->setCircularReferenceHandler(function ($object) {
            
        // return null;
        // });
        $objNormaliser=new ObjectNormalizer(null,null,null,null,null,null,array(ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER=>function ($object) {
            return $object;
        }));
        $normalizers = [ $objNormaliser,new DateTimeNormalizer()];
        $serializer = new Serializer($normalizers ,$encoders);
       
        $jsonContent = $serializer->serialize(["data"=>$content,"msg"=>$msg,"status"=>$status,"remaining"=>$remaining], $type,['ignored_attributes' => $ignoreType]);
        
        $this->headers = new ResponseHeaderBag($headers);
        $this->setContent($jsonContent);
        $this->setStatusCode(200);
        $this->setProtocolVersion('1.0');
    }
}

