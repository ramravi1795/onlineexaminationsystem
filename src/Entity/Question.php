<?php

namespace App\Entity;

use App\Repository\QuestionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=QuestionRepository::class)
 */
class Question
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    private $question;

     /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $option1;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $option2;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $option3;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $answer;
    private $keyAnswer;
    private $questionId;

    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuestion(): ?string
    {
        return $this->question;
    }

    public function setQuestion(?string $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getOption1(): ?string
    {
        return $this->option1;
    }

    public function setOption1(?string $option1): self
    {
        $this->option1 = $option1;

        return $this;
    }

    public function getOption2(): ?string
    {
        return $this->option2;
    }

    public function setOption2(?string $option2): self
    {
        $this->option2 = $option2;

        return $this;
    }

    public function getOption3(): ?string
    {
        return $this->option3;
    }

    public function setOption3(?string $option3): self
    {
        $this->option3 = $option3;

        return $this;
    }


    public function getAnswer(): ?string
    {
        return $this->answer;
    }

    public function setAnswer(?string $answer): self
    {
        $this->answer = $answer;

        return $this;
    }

    public function getQuestionId(): ?int
    {
        return $this->questionId;
    }

    public function setQuestionId(?int $questionId): self
    {
        $this->questionId = $questionId;

        return $this;
    }

    public function getKeyAnswer(): ?string
    {
        return $this->keyAnswer;
    }

    public function setKeyAnswer(?string $keyAnswer): self
    {
        $this->keyAnswer = $keyAnswer;

        return $this;
    }
   
}
